#!/usr/bin/env python

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2
import video
import math
from common import anorm2, draw_str
from time import clock

def angle_between(a,b):
    return np.arctan2(b[1], b[0]) - np.arctan2(a[1], a[0])

class App:
    def __init__(self, video_src):
        self.cam = video.create_capture(video_src)
        print(self.cam)
        self.frame_idx = 0
        self.prev_gray = None
        self.translation_current = np.matrix('0;0');
        self.scale_x = 1
        self.scale_y = 1
        self.rotation = 0

    def run(self):
        while True:
            ret, frame = self.cam.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            vis = frame.copy()

            if self.prev_gray != None:
                affineTransform = cv2.estimateRigidTransform(self.prev_gray, frame_gray, False);

                if(affineTransform != None):
                    translation = affineTransform[:,[2]]
                    self.translation_current = np.add(self.translation_current, translation);

                    scale_x = np.linalg.norm(affineTransform[:,[0]])
                    scale_y = np.linalg.norm(affineTransform[:,[1]])
                    self.scale_x *= scale_x
                    self.scale_y *= scale_y

                    rot1 = np.matrix('0;1')
                    rot2 = np.dot(affineTransform[:,[0,1]], np.matrix('0;1'))
                    self.rotation -= angle_between(rot1.A1, rot2.A1);

                    draw_str(vis, (20, 20), 'TX: ' + str(self.translation_current.item(0,0)))
                    draw_str(vis, (20, 40), 'TY: ' + str(self.translation_current.item(1,0)))
                    draw_str(vis, (20, 60), 'SX: ' + str(self.scale_x))
                    draw_str(vis, (20, 80), 'SY: ' + str(self.scale_y))
                    draw_str(vis, (20, 100), 'RAD: ' + str(self.rotation))
                    draw_str(vis, (20, 120), 'DEG: ' + str(np.rad2deg(self.rotation)))

            self.frame_idx += 1
            self.prev_gray = frame_gray
            cv2.imshow('lk_track', vis)

            ch = 0xFF & cv2.waitKey(1)
            if ch == 27:
                break

def main():
    import sys
    try:
        video_src = sys.argv[1]
    except:
        video_src = 0

    print(__doc__)
    App(video_src).run()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
